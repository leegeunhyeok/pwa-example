# Progressive Web App Example

## 강좌 소개
프로그레시브 웹 앱에 대한 전반적인 내용과 기초 활용법에 대해 학습할 수 있도록 구성한 강좌입니다.  
`웹 개발의 전반적인 이해`가 필요하며 자바스크립트 `ES6`에 대한 내용을 어느정도 알고 계셔야 진행하실 때 어려움이 없습니다.  

- PWA에 대한 기초적인 이해
- 서비스워커에 대한 기초적인 이해
- 브라우저 Fetch 가로채기
- 오프라인 환경에서의 웹 앱 구동을 위한 리소스 캐싱
- 서비스워커 상태 및 이벤트
- Push 알림 기초
- Push 알림 활용
- `Sync.. 준비 중`


## 목차
- [프로그레시브 웹 앱 시작하기 (1) - PWA란?](http://codevkr.tistory.com/85)
- [프로그레시브 웹 앱 시작하기 (2) - 서비스워커 등록](http://codevkr.tistory.com/86)
- [프로그레시브 웹 앱 시작하기 (3) - Fetch, 리소스 캐싱](http://codevkr.tistory.com/87)
- [프로그레시브 웹 앱 시작하기 (4) - 서비스워커 상태](http://codevkr.tistory.com/88)
- [프로그레시브 웹 앱 시작하기 (5) - Push, 푸시 알림 1](#)
- [프로그레시브 웹 앱 시작하기 (6) - Push, 푸시 알림 2](#)

## 이미지 출처
[링크](https://www.boredpanda.com/animals-hybrids-photoshop/?utm_source=google&utm_medium=organic&utm_campaign=organic)

## 지식 공유자
- 이근혁
  - Github: [Geunhyeok LEE](https://github.com/leegeunhyeok)
  - E-Mail: [dev.ghlee@gmail.com](mailto:dev.ghlee@gmail.com)
